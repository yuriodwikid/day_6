package Comp.latihanAbstract;

public class StaffAbs extends WorkerAbs {
    @Override
    public void tambahAbsensi() {
        this.absensi=absensi+1;
    }

    String jabatan;

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public StaffAbs(int idKaryawan, String nama, String jabatan, int absensi) {
        super(idKaryawan, nama, absensi);
        this.jabatan = jabatan;
    }

}
