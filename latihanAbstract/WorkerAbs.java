package Comp.latihanAbstract;

public abstract class WorkerAbs {

    int idKaryawan;
    String nama;
    int absensi;

    public int getAbsensi() {
        return absensi;
    }

    public void setAbsensi(int absensi) {
        this.absensi = absensi;
    }

    public WorkerAbs(int absensi) {
        this.absensi = absensi;
    }

    public int getIdKaryawan() {
        return idKaryawan;
    }

    public void setIdKaryawan(int idKaryawan) {
        this.idKaryawan = idKaryawan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public WorkerAbs(int idKaryawan, String nama, int absensi) {
        this.idKaryawan = idKaryawan;
        this.nama = nama;
        this.absensi = absensi;
    }
    public abstract void tambahAbsensi();

}









