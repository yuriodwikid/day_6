package Comp.tugas2;

public class Staff extends Worker{


    String jabatan;

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public Staff(int idKaryawan, String nama, String jabatan, int inAbsen) {
        super(idKaryawan, nama);
        this.jabatan = jabatan;
    }
}
