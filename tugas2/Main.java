package Comp.tugas2;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Staff> arrStaf = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        int inPilihan;
        do {
        System.out.println("=========================");
        System.out.println("Menu");
        System.out.println("=========================");
        System.out.println("1. Buat Staff");
        System.out.println("2. Tampilkan Staff");
        System.out.println("3. Exit");
        System.out.println("=========================");

            System.out.print("pilihan : ");
            inPilihan = input.nextInt();
            if (inPilihan == 3)
                System.out.println("EXIT");
            else {
                switch (inPilihan) {
                    case 1:
                            System.out.println("Input ID: ");
                            int inID = input.nextInt();
                            System.out.println("Input Nama: ");
                            String inNama = input.next();
                            System.out.println("Input Jabatan: ");
                            String inJabatan = input.next();
                            int inAbsen = 20;
                            Staff staff = new Staff(inID, inNama, inJabatan, inAbsen);
                            arrStaf.add(staff);
                        break;
                    case 2:
                        System.out.println("Tampilkan Laporan Staff");
                        System.out.println("ID\t\t\tNama\t\t\tJabatan");
                        for (Staff s : arrStaf) {
                            System.out.println(s.getIdKaryawan() + "\t\t\t" + s.getNama() + "\t\t\t" + s.getJabatan());
                        }
                        break;
                    default:
                        System.out.println("pilihan");
                        break;
                }
            }
        }while (inPilihan != 3);

    }
}
