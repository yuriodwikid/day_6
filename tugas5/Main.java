package Comp.tugas5;

import Comp.latihanAbstract.StaffAbs;
import Comp.tugas2.Staff;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Staffim> arrStafff = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        int inPilihan;
        do {
            System.out.println("=========================");
            System.out.println("Menu");
            System.out.println("=========================");
            System.out.println("1. Buat Staff");
            System.out.println("2. Absensi Staff");
            System.out.println("3. Laporan Absensi");
            System.out.println("4. Exit");
            System.out.println("=========================");

            System.out.print("pilihan : ");
            inPilihan = input.nextInt();
            if (inPilihan == 4)
                System.out.println("EXIT");
            else {
                switch (inPilihan) {
                    case 1:
                        System.out.println("Input ID: ");
                        int inID = input.nextInt();
                        System.out.println("Input Nama: ");
                        String inNama = input.next();
                        System.out.println("Input Gaji: ");
                        int inGaji = input.nextInt();
                        int inAbsensi = 20;
                        Staffim staff = new Staffim(inID, inNama, inGaji, inAbsensi);
                        arrStafff.add(staff);
                        break;

                    case 2:
                        System.out.println("Input ID");
                        int inIDabsen = input.nextInt();

                        for (Staffim a : arrStafff) {
                            if(a.idKaryawan == inIDabsen){
                                System.out.println("id anda = "+a.idKaryawan);
                                System.out.println("nama anda = "+a.nama);
                                a.tambahAbsensiStaff();
                                System.out.println("Absen sukses");
                            }
                        }
                    case 3:
                        System.out.format("+----------------------------------------+\n");
                        System.out.format("|              Laporan Absensi           |\n");
                        System.out.format("+----------------------------------------+\n");
                        String tbl = "| %-7s | %-8s | %-7s | %-7s |%n";
                        System.out.format(tbl,"ID","Nama","Gaji","Absensi");
                        System.out.format("+----------------------------------------+\n");

                        for (Staffim s : arrStafff) {
//                            for (int i = 0; i < arrStafff.size(); i++) {
                                System.out.format(tbl, s.getidKaryawan(), s.getNama(), s.getGajiPokok(), s.getAbsensiHari());
                                System.out.println("+----------------------------------------+");
                            }
                    default:
                        System.out.println("salah input");
                        }
            }

        }
        while (inPilihan != 4) ;
    }
}



