package Comp.tugas1;

import Comp.tugas1.Siswa;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Tugas1 {
    public static void main(String[] args) throws Exception {
        {
            ArrayList<Siswa> alSiswa = new ArrayList<>();

            Scanner input = new Scanner(System.in);
            int pilihanMenu;

            do {
                System.out.println("Menu");
                System.out.println("1. Input data Mahasiswa");
                System.out.println("2. Tulis Laporan ke File TXT");
                System.out.println("3. EXIT");

                System.out.print("Pilihan kamu = ");
                pilihanMenu = input.nextInt();

                switch (pilihanMenu) {
                    case 1:
                        System.out.println("FROM FILE");
                        System.out.println("=========");
                        System.out.println("Input Directory:");
                        String inputDirectoryRead = input.next();
                        System.out.println("Input Filename:");
                        String inputFileName = input.next();
///Users/ada-nb185/Documents
                        String file = inputDirectoryRead+inputFileName;
                        FileReader fr = new FileReader(file);

                        int i;
                        StringBuilder strAccu = new StringBuilder();
                        System.out.println("\n"+inputFileName);
                        while((i=fr.read()) != -1) {
                            strAccu.append((char) i);
                        }
                        // parsing data
                        String[] strAccuParsed = strAccu.toString().split("\n");

                        for(int j=0; j<strAccuParsed.length; j++) {
                            System.out.println(strAccuParsed[j]);
                            if(j > 0) {
                                String[] strAccuParsed2 = strAccuParsed[j].trim().split(",");

                                int id = Integer.parseInt(strAccuParsed2[0]);
                                String nama = strAccuParsed2[1];
                                int nilai = Integer.parseInt(strAccuParsed2[2]);

                                Siswa s = new Siswa(id, nama, nilai);
                                alSiswa.add(s);
                            }
                        }

                        fr.close();
                        break;
                    case 2:
                        System.out.println("Tulis Laporan");
                        try {
                            System.out.println("Masukan directory file:");
                            String inputDirectoryWrite = input.next();
                            System.out.println("Masukan nama file dan format:");
                            String inputNamaFileWrite = input.next();
                            FileWriter fw = new FileWriter(inputDirectoryWrite + "/" + inputNamaFileWrite);
                            for (Siswa s : alSiswa) {
                                fw.write("id : " + s.getId());
                                fw.write("\n");
                                fw.write("nama : " + s.getNama());
                                fw.write("\n");
                                fw.write("nilai : " + s.getNilai());
                                fw.write("\n\n");
                            }
                            fw.close();
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        System.out.println("Success...");

                }
            }while(pilihanMenu != 3);


        }
    }
}