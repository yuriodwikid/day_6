package Comp.latihanAbstract;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<StaffAbs> arrStaf = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        int inPilihan;
        do {
        System.out.println("=========================");
        System.out.println("Menu");
        System.out.println("=========================");
        System.out.println("1. Buat Staff");
        System.out.println("2. Absensi");
        System.out.println("3. Tampilkan Staff");
        System.out.println("4. Exit");
        System.out.println("=========================");

            System.out.print("pilihan : ");
            inPilihan = input.nextInt();
            if (inPilihan == 4)
                System.out.println("EXIT");
            else {
                switch (inPilihan) {
                    case 1 -> {
                        System.out.println("Input ID: ");
                        int inID = input.nextInt();
                        System.out.println("Input Nama: ");
                        String inNama = input.next();
                        System.out.println("Input Jabatan: ");
                        String inJabatan = input.next();
                        System.out.println("Input Absen: ");
                        int inAbsen = input.nextInt();
                        StaffAbs staff = new StaffAbs(inID, inNama, inJabatan, inAbsen);
                        arrStaf.add(staff);
                    }
                    case 2 -> {
                        System.out.println("Input ID");
                        int inIDabsen = input.nextInt();

                        for (StaffAbs a : arrStaf) {
                            if(a.idKaryawan == inIDabsen){
                                System.out.println("id anda = "+a.idKaryawan);
                                System.out.println("nama anda = "+a.nama);
                                a.tambahAbsensi();
                                System.out.println("Absen sukses");
                            }
                        }
                    }
                    case 3 -> {
                        System.out.format("+----------------------------------------+\n");
                        System.out.format("|         Tampilkan Laporan Staff        |\n");
                        System.out.format("+----------------------------------------+\n");
                        String tbl = "| %-7s | %-8s | %-7s | %-7s |%n";
                        System.out.format(tbl,"ID","Nama","Jabatan","Absensi");
                        System.out.format("+----------------------------------------+\n");

                        for (StaffAbs s : arrStaf) {
                            for (int i = 0; i < arrStaf.size(); i++) {
                                System.out.format(tbl, s.getIdKaryawan(), s.getNama(), s.getJabatan(), s.getAbsensi());
                                System.out.format("+----------------------------------------+\n");
                                System.out.println("\n");
                            }
                            }
                    }
                    default -> System.out.println("Salah input");
                }
            }
        }while (inPilihan != 4);
    }
}
