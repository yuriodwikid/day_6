package Comp.tugas3;

public class Child extends Parent{
    String name = "Tom";
    int money = 200;

    @Override
    public void car() {
        System.out.println("Child's car");
    }

    public void parentInfo(){
        System.out.println("parent's name = " + super.name);
        System.out.println("parent's money= $" + super.money);
    }

    public void childInfo(){
        System.out.println("name = " + name);
        System.out.println("money = $" + money);
    }
}
